#!/bin/sh

set -e

oc process -f configmap.yaml $@ | oc create -f -

for f in volume_claims/*.yaml; do
    oc create -f $f
done

for f in deployment_configs/*.yaml; do
  oc create -f $f
done

for f in services/*.yaml; do
  oc create -f $f
done

oc create -f route.yaml

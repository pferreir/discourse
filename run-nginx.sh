#!/bin/bash
# If there are files in /tmp/nginx-configmap that are not empty
# (overriden by a ConfigMap) copy them to /etc/nginx/conf.d/
if [ -n "$(ls -A /tmp/discourse-nginx-configmap)" ]
then
  for f in /tmp/discourse-nginx-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-nginx-configmap/* /etc/nginx/
    fi
  done
fi

exec service nginx start
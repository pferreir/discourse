FROM ruby:2.4
MAINTAINER web-services-core <web-services-core@cern.ch>

# Install prerequisites #############################
RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install gettext

# Set up Discourse #############################
ENV DISCOURSE_RELEASE v1.8.3
ENV RAILS_ENV production

RUN mkdir -p discourse

ENV HOME /discourse

RUN git clone --depth=1 --single-branch --branch "$DISCOURSE_RELEASE"  https://github.com/discourse/discourse.git /discourse/app

WORKDIR $HOME/app
RUN mkdir -p ./public/plugins/ && \
    mkdir -p ./tmp/sockets/ && \
    mkdir -p ./tmp/pids/

# Plugins - OAuth2
RUN git clone --depth=1 https://github.com/discourse/discourse-oauth2-basic.git /discourse/app/plugins/discourse-oauth2-basic

RUN exec bundle install --no-cache --no-prune --deployment --without development:test
RUN exec bundle clean -V

ADD ["run-discourse.sh","run-sidekiq.sh","./"]
RUN chmod +x ./run-discourse.sh ./run-sidekiq.sh

RUN chmod -R a+rw /discourse

# entrypoint will be overriden for the nginx container
ENTRYPOINT ["./run-discourse.sh"]

EXPOSE 8080

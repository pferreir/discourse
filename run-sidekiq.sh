#!/bin/bash
# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /discourse/app/config
if [ -n "$(ls -A /tmp/discourse-configmap)" ]
then
  for f in /tmp/discourse-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-configmap/* /discourse/app/config/
    fi
  done
fi

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/app/config/discourse.conf
echo "--> DONE"

echo "--> Running sidekiq ..."
bundle exec sidekiq -C config/sidekiq.yml -e production